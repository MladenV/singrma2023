package rs.ac.singidunum.android.v11;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class Database extends SQLiteOpenHelper {
    private static final String DATABASE_NAME="baza.sqlite";

    public Database(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //kreiranje baze
        //CREATE TABLE oglas (oglas_id INTEGER PRIMARY KEY AUTOINCREMENT, naslov TEXT, cena REAL);

        String SQL = String.format("CREATE TABLE IF NOT EXISTS %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT, %s REAL);",
                Oglas.TABLE_NAME, Oglas.FIELD_OGLAS_ID, Oglas.FIELD_NASLOV, Oglas.FIELD_CENA);

        db.execSQL(SQL);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //implementacija promene baze iz jedne verzije u drugu
        //ovde bi trebalo implementirati tranziciju
        db.execSQL(String.format("DROP TABLE IF EXISTS %s;", Oglas.TABLE_NAME));
        //kreiranje nove tabele
        onCreate(db);
    }

    public void addOglas(String naslov, double cena){
        SQLiteDatabase db = this.getWritableDatabase();
        //da bismo uneli podatke u bazu, treba nam skup vrednosti, ContentValues
        ContentValues cv = new ContentValues();

        cv.put(Oglas.FIELD_NASLOV, naslov);
        cv.put(Oglas.FIELD_CENA, cena);
        //radimo insert ovog skupa podataka
        //nullColumnHack navodi ime kolone u kojoj ce eksplicitno biti unesen NULL ako je CV prazan
        db.insert(Oglas.TABLE_NAME, null, cv);
    }

    public void editOglas(int oglasId, String naslov, double cena){
        SQLiteDatabase db = this.getWritableDatabase();
        //da bismo uneli podatke u bazu, treba nam skup vrednosti, ContentValues
        ContentValues cv = new ContentValues();

        cv.put(Oglas.FIELD_NASLOV, naslov);
        cv.put(Oglas.FIELD_CENA, cena);
        //radimo update baze, prosledjujemo tabelu, skup podataka, diskriminator u formi prep. statementa za pronalazenje torke
        //i konacno kao niz argumenata vrednosti koje se ubacuju u prep.statement
        db.update(Oglas.TABLE_NAME, cv, Oglas.FIELD_OGLAS_ID + "=?", new String[] {String.valueOf(oglasId)});
    }

    public int deleteOglas(int oglasId){
        int numDeleted = 0;

        SQLiteDatabase db = this.getWritableDatabase();
        numDeleted = db.delete(Oglas.TABLE_NAME, Oglas.FIELD_OGLAS_ID + "=?", new String[] {String.valueOf(oglasId)});
        return numDeleted;
    }

    public Oglas getOglasById(int oglasId){
        SQLiteDatabase db = this.getReadableDatabase();
        // SELECT * FROM oglas WHERE oglas_id = ?
        String query = String.format("SELECT * FROM %s WHERE %s = ?", Oglas.TABLE_NAME, Oglas.FIELD_OGLAS_ID);
        Cursor result = db.rawQuery(query, new String[] {String.valueOf(oglasId)});
        if(result.moveToFirst()) { //ako ima
            String naslov = result.getString(result.getColumnIndex(Oglas.FIELD_NASLOV)); //prima columnIndex odnosno redni broj kolone
            Double cena = result.getDouble(result.getColumnIndex(Oglas.FIELD_CENA));
            return new Oglas(oglasId, naslov, cena);
        } else{
            return null;
        }
    }

    public List<Oglas> getAllOglasi(){
        SQLiteDatabase db = this.getReadableDatabase();
        String query = String.format("SELECT * FROM %s", Oglas.TABLE_NAME);
        Cursor result = db.rawQuery(query, null);
        result.moveToFirst();
        List<Oglas> list = new ArrayList<Oglas>(result.getCount()); //kreiramo listu sa onoliko elemenata koliko je nadjeno
        while(!result.isAfterLast()){ //dok nismo izasli posle poslednjeg
            int oglasId = result.getInt(result.getColumnIndex(Oglas.FIELD_OGLAS_ID));
            String naslov = result.getString(result.getColumnIndex(Oglas.FIELD_NASLOV));
            double cena= result.getDouble(result.getColumnIndex(Oglas.FIELD_CENA));

            //kreiramo objekat i dodajemo u listu
            list.add(new Oglas(oglasId, naslov, cena));
            //pomeramo kursor
            result.moveToNext();
        }
        return list;
    }
}
