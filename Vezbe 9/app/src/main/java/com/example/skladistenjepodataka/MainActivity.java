package com.example.skladistenjepodataka;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class MainActivity extends AppCompatActivity {

    private final static String SHARED_PREFERENCES_PREFIX = "MainActivitySharedPreferencesPrefix";
    private final static String SHARED_PREFERENCES_IME = "ime";
    private final static String SHARED_PREFERENCES_EMAIL = "email";

    private final static String APP_DATA_PREFIX = "MainActivityAppDataPrefix";

    private final static String DATA_PREFIX = "MainActivityDataDir";
    private final static String DATA_FILE = "/podaci-korisnika.txt";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sharedPrefs();
        internalStorage();
        try {
            externalStorage();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void sharedPrefs(){
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFERENCES_PREFIX, 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(SHARED_PREFERENCES_IME, "ime");
        editor.commit();

        System.out.println(sharedPreferences.getString(SHARED_PREFERENCES_IME, "default"));
    }

    private void internalStorage(){
        try{
            FileOutputStream f = openFileOutput(APP_DATA_PREFIX, Context.MODE_PRIVATE);
            PrintWriter pw = new PrintWriter(f);
            pw.println("ime internal");
            pw.println("prezime internal");
            pw.flush();
            pw.close();
            f.close();
        }
        catch(Exception e)
        {
            //handle exception
        }

        try {
            FileInputStream f = openFileInput(APP_DATA_PREFIX);
            BufferedReader bf = new BufferedReader(new InputStreamReader(f));

            String ime = bf.readLine();
            String email = bf.readLine();

            bf.close();
            f.close();

            System.out.println(ime);

        }
        catch(Exception e){
            //handle exception
        }
    }

    private void externalStorage() throws IOException {
        String ime = "Ime external";

        File dir = null;
        if(Build.VERSION.SDK_INT >= 19){
            dir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), DATA_PREFIX);
        }else{
            dir = new File(Environment.getExternalStorageDirectory() + "/Documents");
        }

        dir.mkdirs();

        File f = new File(dir.getAbsolutePath() + DATA_FILE);
        try {
            PrintWriter pw = new PrintWriter(f);
            pw.println(ime);
            pw.flush();
            pw.close();
        }catch(Exception e){
            //handle exception
        }

        
    }



}