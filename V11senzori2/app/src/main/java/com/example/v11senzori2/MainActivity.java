package com.example.v11senzori2;

import androidx.appcompat.app.AppCompatActivity;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    TextView label;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initComponents();
        initSensor();
    }

    private void initComponents(){
        label = findViewById(R.id.label);
    }

    private void initSensor(){
        SensorManager sm = (SensorManager) getSystemService(SENSOR_SERVICE);
        Sensor s = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sm.registerListener(this,s, 100);
    }

    //detektcija univerzalnog SensorEvent-a
    @Override
    public void onSensorChanged(SensorEvent event) {
        label.setText(String.format("%10.4f \n %10.4f \n %10.4f\n", event.values[0], event.values[1], event.values[2]));


    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}