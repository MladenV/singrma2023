package com.example.fragmentinapredno;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
//ovaj fragment nema reference na bilo sta drugo sem viewModela koji koristi
//iako vrsi komunikaciju sa drugim fragmentom, ne zna za njega
//takodje main activity nema ulogu cvora za komunikaciju
public class DetailsFragment extends Fragment {
    ViewModelExample viewModel;

    public DetailsFragment() {
        // Required empty public constructor
    }

    public static DetailsFragment newInstance() {
        DetailsFragment fragment = new DetailsFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_details, container, false);
        //pri povezivanju na viewModel kao lifecycleOwner prosledjujemo activity na koji je vezan
        //tako se obezbedjuje da dobija isti viewModel kao i ostali fragmenti (ako svi na isti nacin koriste)
        viewModel = new ViewModelProvider(requireActivity()).get(ViewModelExample.class);
        //kada god se promeni viewModel, odnosno njegovo svojstvo selected, poziva se metoda setLabel
        viewModel.getSelected().observe(getViewLifecycleOwner(), selected ->{
            setLabel(v, selected);
        });
        return v;
    }

    private void setLabel(View v, String s){
        ((TextView)v.findViewById(R.id.detailsTextView)).setText(s);
    }

}