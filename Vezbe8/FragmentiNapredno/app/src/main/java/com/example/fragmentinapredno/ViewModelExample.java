package com.example.fragmentinapredno;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//ViewModel koji se ovde kreira, iako sluzi za azuriranje svih fragmenata koji ga koriste,
//kao i komunikaciju izmedju njih, ne zna za fragmente i ne zna za main activity
//ne treba da ima referencu na bilo koji element koji ima zivotni ciklus, jer treba da postoji nezavisno od njih
public class ViewModelExample extends ViewModel {
    //mutable live data se moze promeniti, live data ne moze
    private MutableLiveData<List<String>> data;
    private MutableLiveData<String> selectedData = new MutableLiveData<>();

    public LiveData<List<String>> getData(){
        //ukoliko zahtevani podaci nisu učitani, instanciramo data i dobavljamo podatke
      if(data == null){
          data = new MutableLiveData<List<String>>();
          fetchData(null);
      }

      return data;
    };

    //mehanizam za komunikaciju, imamo 2 metode
    //prva metoda, kada se selektuje item
    public void selectData(String item){
        this.selectedData.setValue(item);
    }
    //druga metoda, kada se zahteva selektovani item
    public LiveData<String> getSelected(){
        return this.selectedData;
    }

    public void fetchData(List<String> newData){
        //ovde bismo dobavljali podatke preko API-a
        if(newData == null){
            newData = new ArrayList<>(Arrays.asList("Prvi Fakultet", "Drugi Fakultet", "Treci Fakultet"));
        }
        this.data.setValue(newData);
    }
}
