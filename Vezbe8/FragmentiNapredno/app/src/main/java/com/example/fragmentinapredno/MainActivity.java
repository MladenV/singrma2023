package com.example.fragmentinapredno;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.widget.Button;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //dobavljanje podataka iz viewmodel-a
        //ako se rekreira activity, dobija istu instancu viewmodela
        //NAPOMENA: mogli bismo svu logiku da izbacimo iz main activity-a, tako što uopšte ne instanciramo ovde ni viewmodel
        //niti ubacujemo bilo kakvu funkcionalnost za dugme
        //Activity moze da ima iskljucivo kod za fragmente i navigaciju (smenjivanje fragmenata)
        ViewModelExample vm = new ViewModelProvider(this).get(ViewModelExample.class); //owner predstavlja scope na osnovu kojeg se deli ista instanca
        /*vm.getData().observe(this, new Observer<List<String>>() {
            @Override
            public void onChanged(List<String> data) {
                //azuriraj UI
            }
        });*/
         //ekvivalentno sa
        /*vm.getData().observe(this, data ->{
           azuriraj UI
        });*/

        //postavljanje fragmenata
        //pri dodavanju fragmenata im se ne prosleđuju nikakvi podaci, activity ne mora nikakve podatke da dostavlja niti da ima bilo kakvu poslovnu logiku
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        MasterFragment m = MasterFragment.newInstance();
        DetailsFragment d = DetailsFragment.newInstance();
        ft.add(R.id.prviFragment, m, null);
        ft.add(R.id.drugiFragment, d, null);
        ft.commit();

        //ova funkcionalnost "azuriranja" podataka je dodata primera radi, fragment za fakultete bi imao opciju za azuriranje
        //ili bi se navigiralo na novi fragment za dodavanje fakulteta
        ((Button)findViewById(R.id.button)).setOnClickListener(view ->{
            vm.fetchData(new ArrayList<String>(Arrays.asList("Novi Fakultet1", "Novi Fakultet2")));
        });

    }
}