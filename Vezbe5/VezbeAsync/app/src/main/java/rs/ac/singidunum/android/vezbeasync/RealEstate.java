package rs.ac.singidunum.android.vezbeasync;

import org.json.JSONObject;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class RealEstate {
    private String id, title, type, price, furnished, area, extras;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getFurnished() {
        return furnished;
    }

    public void setFurnished(String furnished) {
        this.furnished = furnished;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getExtras() {
        return extras;
    }

    public void setExtras(String extras) {
        this.extras = extras;
    }

    public RealEstate(String id, String title, String type, String price, String furnished, String area, String extras) {
        this.id = id;
        this.title = title;
        this.type = type;
        this.price = price;
        this.furnished = furnished;
        this.area = area;
        this.extras = extras;
    }

    public RealEstate(){

    }

    public static RealEstate fromJson(JSONObject o){
        RealEstate r = new RealEstate();
        try{
            if(o.has("id")){
                r.setId(o.getString("id"));
            }
            if(o.has("price")){
                r.setPrice(o.getString("price"));
            }
            if(o.has("type")){
                r.setType(o.getString("type"));
            }
            if(o.has("extras")){
                r.setExtras(o.getString("extras"));
            }
            if(o.has("furnished")){
                r.setFurnished(o.getString("furnished"));
            }
            if(o.has("area")){
                r.setId(o.getString("area"));
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return r;
    }

    public static RealEstate convertJson(String json){
        RealEstate r = new RealEstate();

        r = new Gson().fromJson(json, RealEstate.class);

        return r;
    }
}
