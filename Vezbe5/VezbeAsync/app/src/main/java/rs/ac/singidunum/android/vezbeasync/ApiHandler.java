package rs.ac.singidunum.android.vezbeasync;

import android.os.AsyncTask;
import android.view.View;
import android.widget.ProgressBar;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class ApiHandler {

    public static void getJSON(String address, final ProgressBar pb, final ProgressBar pb2, final ReadDataHandler rdh){
        AsyncTask<String, Integer, String> task = new AsyncTask<String, Integer, String>() {
            //pri kreiranju AsyncTask se prosleđuju tri tipa podataka, Params, Progress i Result
            @Override
            protected String doInBackground(String... strings) {
                String response = "";

                //dobavljanje konekcije
                try{
                    URL url = new URL(strings[0]); //prvi parametar
                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    String red;
                    int i = 0;
                    while((red=br.readLine()) != null){
                        i+=1;
                        response += red;
                        Thread.sleep(1000);
                        publishProgress(i);
                    }
                    br.close();
                    con.disconnect();

                }catch(Exception e){
                    e.printStackTrace();
                }

                return response;
            }

            @Override
            protected void onPostExecute(String response){
                rdh.setJson(response);
                pb2.setVisibility(View.INVISIBLE);
                rdh.sendEmptyMessage(0);
            }

            @Override
            protected void onProgressUpdate(Integer... values){
                //
                pb.setProgress(values[0] * 33);
            }


        };
        task.execute(address);
    }
}
