package rs.ac.singidunum.android.vezbefragmenti;

import android.view.View;

public interface CustomInteraction {
    public void onItemClicked(View view);
}
