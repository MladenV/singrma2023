package rs.ac.singidunum.android.vezbefragmenti;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements CustomInteraction {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Dobavljamo podatke - api je mockovan
        ArrayList<String> data = FakultetiAPI.getAll();

        //dobavljamo fragment manager
        FragmentManager fm = getSupportFragmentManager();

        //kreiramo transakciju
        FragmentTransaction ft = fm.beginTransaction();

        //izvrsavamo operacije unutar transakcije
        //instanciramo fragment, pri instanciranju mu prosleđujemo podatke - pogledati newInstance
        FakultetiFragment ff = FakultetiFragment.newInstance(data);

        //dodajemo fragment u activity, prosledjujemo mu container, instancu i tag po kojem kasnije
        //mozemo da pronadjemo fragment
        ft.add(R.id.fakulteticontainer, ff, "fakulteti");
        //izvrsavamo transakciju
        ft.commit();

        TextView tv = findViewById(R.id.textfield);
        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //pronalazimo fragment po tag-u i pozivamo njegovu metodu
                ((FakultetiFragment)getSupportFragmentManager()
                        .findFragmentByTag("fakulteti")).manipulisiFragmentom();
                System.out.println("pritisnuto");

                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                FakultetiFragment trenutni = (FakultetiFragment) getSupportFragmentManager()
                        .findFragmentByTag("fakulteti");
                //uklanjamo fragment
                ft.remove(trenutni);
                //ako dodamo transakciju na back stack, pritiskom dugmeta back se revertuje
                //to se odnosi na CELU transakciju - moguce je unutar jedne transakcije izvrsiti
                //dodavanje, brisanje ili razmenu (replace) vise fragmenata
                ft.addToBackStack(null);
                ft.commit();

            }
        });

    }

    @Override
    public void onItemClicked(View view) {
        //reakcija na event iz fragmenta
    }
}
