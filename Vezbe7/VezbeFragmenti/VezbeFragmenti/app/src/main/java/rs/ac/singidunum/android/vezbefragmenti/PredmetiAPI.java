package rs.ac.singidunum.android.vezbefragmenti;

import java.util.ArrayList;


public class PredmetiAPI {
    public static ArrayList<String> getAll(String fakultet){
        ArrayList<String> predmeti = new ArrayList<>();
        for(int i = 0; i < 20; i++){
            predmeti.add(fakultet + "-Predmet" + i);
        }
        return predmeti;
    }
}
